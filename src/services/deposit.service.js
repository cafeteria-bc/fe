import axios from 'axios';
import config from '@/app.config';
import DateService from './date.service';
import HttpService from './http.service';

export default class DepositService {
    constructor() {
        this.dateService = new DateService();
        this.httpService = new HttpService();
    }

    async save(depositsByPockets) {
        try {
            (await axios.post(config.baseUrl + '/deposits/bulk', depositsByPockets));
        } catch (err) {
            if (err.response) {
                alert(err.response.data.totalAmount)
            }
        }
    }

    async getAll() {
        return (await axios.get(config.baseUrl + '/deposits')).data.data;
    }

    async download() {
        const response = await axios({
            url: config.baseUrl + '/deposits/export',
            method: 'GET',
            responseType: 'blob'
        });

        this.httpService.forceDownload(response.data, 'deposits.csv');
    }

    convertToGridData(deposits, pockets) {
        const result = {};
        for (const pocket of pockets) {
            result[pocket.id] = {};
        }

        for (const deposit of deposits) {
            result[deposit.pocket.id][deposit.month] = deposit.amount;
        }

        for (const pocket of pockets) {
            for (const month of this.dateService.getMonths()) {
                if (!(month in result[pocket.id])) {
                    result[pocket.id][month] = 0;
                }
            }
        }

        return result;
    }
}