import axios from 'axios';
import config from '@/app.config';

export default class PocketService {
    async getAll() {
        return (await axios.get(config.baseUrl + '/pockets')).data.data
    }
}