export default class DateService {
    getMonths() {
        const months = [];
        for (let i = 1; i <= 12; i++) {
            months.push(`${new Date().getFullYear()}-${i.toString().padStart(2, '0')}`);
        }

        return months;
    }
}